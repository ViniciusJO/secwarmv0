# { SecWarn }
# Sistema de Aviso de Aproximação entre Pessoas e Equipamentos de Mineração

## Proposta

[...]

## BLE Structure
### O que é?
BLE (Bluetooth Low Energy) é um protocolo de transferencia de dados sem fio desenvolvido a partir da tecnologia Bluetooth tradicional mas com foco no baixo consumo energético. Implementado a partir do padrão Bluetooth core versão 4.0.
### Protocolo
Baseado em uma estrutura de central/periféricos (ou ainda host/devices ou servidor/cliente) o processo de conexão é inicializada com uma requisição da central que entende a existencia dos periféricos atravez do processo de advertising feito pelos periféricos, uma anunciação no estilo broadcast que estabelece o que denomina-se General Access Point (GAP). Este processo é caracterizador do baixo consumo de recursos do BLE, uma vez que não há continuo broadcast de dados, sendo configuravel em frequencia de anunciação e potência do sinal transmitido. A central então tem a opção de requisitar uma resposta de escaneamento, uma forma de trocar mais informações antes mesmo da conxão dedicada de serviço ser estabelecida.
Então a central pode estabelecer uma conexão dedicada com o periférico, que por padrão, então, deixa de se comportar como GAP, interrompendo o advertising e se tornando indetectavel à outros hosts. Esta conexão dedicada é caracterizada pelo Generic Attribute Profile (GATT) que define como sera feito o intercambio de dados entre central e periférico baseado no conceito de serviços (services) e características (characteristics). Se utilizando do protocolo genérico Attribute Protocoll (ATT), armazena os dados e informações dos services e characteristics em uma lookup table indexada com IDs de 16 bits.
#### GAP
##### Advertising
##### Scan Response
#### `Broadcast Network`
#### GATT
##### Services
##### Characteristics
#### Padrões de mercado
##### Wibree
##### iBeacon
##### Eddystone

## Implementação
### ESP32
#### Hardware
#### Código
